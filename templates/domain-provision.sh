#!/bin/bash

# for dns-backend, we hardcode SAMBA_INTERNAL here
# if samba_dns_backend was specified differently, e.g.: BIND9_DLZ
# we will use samba_upgradedns to switch dns backend after provision

/usr/local/samba/bin/samba-tool domain provision \
  --use-rfc2307 \
  --realm={{samba_realm}} \
  --domain={{samba_domain}} \
  --server-role=dc \
  --dns-backend=SAMBA_INTERNAL \
  --backend-store={{samba_backend_store}} \
  --host-ip='{{primary_dc_ip}}' \
  --adminpass='{{samba_password}}' \
  --krbtgtpass='{{samba_password}}' \
  --machinepass='{{samba_password}}' \
  --dnspass='{{samba_password}}' \
  --option='dns forwarder={{samba_dns_forwarder}}' \
  --option='kccsrv:samba_kcc={{use_samba_kcc}}' \
  --option='ldapserverrequirestrongauth=no' \
  --option='winbind enum users = yes' \
  --option='winbind enum groups = yes' \
  "$@"
